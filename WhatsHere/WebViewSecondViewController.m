//
//  WebViewSecondViewController.m
//  ASBA
//
//  Created by Sabin.MS on 29/07/15.
//  Copyright (c) 2015 Jimmy. All rights reserved.
//

#import "WebViewSecondViewController.h"
#import "MBProgressHUD.h"
#import "ECSlidingViewController.h"
#import "SlidingMenuViewController.h"



@interface WebViewSecondViewController ()
{
    MBProgressHUD *HUD;
}

@end

@implementation WebViewSecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_btnBack setTitle:[NSString stringWithFormat:@"< back"] forState:UIControlStateNormal];
    _labelHeading.text=_strTitle;
    NSURL *url=[NSURL URLWithString:_stringURL];
    NSURLRequest *urlRequest=[NSURLRequest requestWithURL:url];
    //_btnBack.titleLabel.text=_strTitle;
    [_webView loadRequest:urlRequest];
    HUD=[[MBProgressHUD alloc]initWithView:self.view];
    
   
    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated{
    [self setTitle:_strTitle];
    //[_btnBack setTitle:[NSString stringWithFormat:@"< %@",_strTitle] forState:UIControlStateNormal];

}
- (IBAction)backBtn:(id)sender {
   // [self.navigationController popToRootViewControllerAnimated:YES];
    [self.navigationController popViewControllerAnimated:YES];
//    ECSlidingViewController *obj=[self.storyboard instantiateViewControllerWithIdentifier:@"ECSlidingViewController"];
//   // obj.btnToshowTag=_btnTag;
//    [self.navigationController pushViewController:obj animated:YES];


  
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    [HUD show:YES];
    [self.view addSubview:HUD];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [HUD hide:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
