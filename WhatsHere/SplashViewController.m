//
//  SplashViewController.m
//  ASBA
//
//  Created by Sabin.MS on 31/07/15.
//  Copyright (c) 2015 Jimmy. All rights reserved.
//

#import "SplashViewController.h"
#import "ECSlidingViewController.h"

@interface SplashViewController ()

@end

@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self performSelector:@selector(NextView) withObject:nil afterDelay:3.0];
    [_activityIndicator performSelector:@selector(startAnimating) withObject:nil afterDelay:0.1];
    [_activityIndicator performSelector:@selector(stopAnimating) withObject:nil afterDelay:2.9];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
    // Dispose of any resources that can be recreated.
}
-(void)NextView{
    ECSlidingViewController *obj=[self.storyboard instantiateViewControllerWithIdentifier:@"ECSlidingViewController"];
    [self.navigationController pushViewController:obj animated:YES];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
